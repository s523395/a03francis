
var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser")
var engines = require("consolidate")

var app = express() // make express app
var server = require('http').createServer(app) 
const expressLayout = require('express-ejs-layouts')

// set up the view engine
app.set("views", path.join(__dirname, "views")) 
app.set("view engine", "ejs") 
app.engine('ejs',engines.ejs)

// manage our entries
var entries = []
app.locals.entries = entries 

app.use(express.static(path.join(__dirname,'assets'),{maxAge: 31557600000}))
app.use(expressLayout)

// set up the logger
app.use(logger("dev")) 
app.use(bodyParser.urlencoded({ extended: false }))

// GETS
app.get("/", function (request, response) {
    response.render("index.ejs")
})
app.get("/about",function (request, response){
    response.render("about.ejs")
})
app.get("/contact",function(request,response){
    response.render("contact.ejs")
})
app.get("/guestbook",function(request,response){
    response.render("guestbook.ejs")
})
app.get("/new-entry", function (request, response) {
response.render("new-entry.ejs")
})

// POSTS
app.post("/contact",function(request,response){
    
})
app.post("/new-entry", function (request, response) {
if (!request.body.title || !request.body.body) {
response.status(400).send("Entries must have a title and a body.")
return
}
entries.push({ //store it
title: request.body.title,
content: request.body.body,
published: new Date()
})
response.redirect("/") //redirect to main page
})

//404
app.use(function (request, response) {
response.status(404).render("404")
})

//Listen for an application request on port 8081
server.listen(8081, function () {
console.log('Guestbook app listening on http://127.0.0.1:8081/')
})